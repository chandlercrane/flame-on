mapboxgl.accessToken = 'pk.eyJ1IjoiY2hhbmRsZXJjcmFuZSIsImEiOiJjanQwcTNhaWEwM2x2M3pwM2I4Nm40Nmo2In0.82_w067nYHhuvrsL3kuHUw';
var map = new mapboxgl.Map({
  container: 'map', // container id
  style: 'mapbox://styles/chandlercrane/cjt0qcy7d01ya1fmmcdis1lis',
  center: [-86.25, 41.70], // starting position [lng, lat]
  zoom: 11 // starting zoom
});

const testtext = document.getElementById('testtext')

var size = 50;

var dot = {
  width: size,
  height: size,
  data: new Uint8Array(size * size * 4),

  onAdd: function() {
    var canvas = document.createElement('canvas');
    canvas.width = this.width;
    canvas.height = this.height;
    this.context = canvas.getContext('2d');
  },

  render: function() {
    var duration = 1000;
    var t = (performance.now() % duration) / duration;

    var radius = size / 2 * .8;
    var outerRadius = size / 2 * 0.7 * t + radius;
    var context = this.context;

    // draw inner circle
    context.beginPath();
    context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
    context.fillStyle = 'rgba(255, 100, 100, 1)';
    //context.strokeStyle = 'rgba(80, 122, 142, 1)';
    context.strokeStyle = 'white';
    context.lineWidth = 3;
    context.fill();
    context.stroke();

    // update this image's data with data from the canvas
    this.data = context.getImageData(0, 0, this.width, this.height).data;

    // keep the map repainting
    map.triggerRepaint();

    // return `true` to let the map know that the image was updated
    return true;
  }
};


var num_points = 0
var data_points = {
  "id": "dots",
  "type": "symbol",
  "source": {
    "type": "geojson",
    "data": {
      "type": "FeatureCollection",
      "features": []
    }
  },
  "layout": {
    "icon-image": "dot"
  }
}

createData = function(lon, lat, name, address, c, desc, desc_raw){
  data_points.source.data.features.push(
    {
      "type": "Feature",
      "properties": {
        "name": name,
        "address": address,
        "class": c,
        "description": desc,
        "description_raw": desc_raw
      },
      "geometry": {
        "type": "Point",
        "coordinates": [lon, lat]
      }
    }
  )
}

// Get DOM Elements
const problemButtons = document.getElementsByClassName("problem")

const addPointButton = document.getElementById("addPointButton")
const personNameInput = document.getElementById("personNameInput")
const addressInput = document.getElementById("addressInput")
const descriptionInput = document.getElementById("descriptionInput")

const totalCases = document.getElementById("totalCases")
const dataBlockName = document.getElementById("dataBlockName")
const dataBlockAddress = document.getElementById("dataBlockAddress")
const dataBlockDesc = document.getElementById("dataBlockDesc")

const removePoint = document.getElementById("removePoint")

function propogateDataBlock(name, address, cc, desc_raw){
  dataBlockName.innerText = name
  dataBlockAddress.innerText = address
  dataBlockDesc.innerText = desc_raw
}



// Get DB stuff
database = firebase.database()

function getPoints(){
  var leadsRef = database.ref('points');
  leadsRef.on('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var child = childSnapshot.val()
        var desc_raw = child.description
        var description = "<strong>" + child.name + "</strong>"
        description = description + "</br><i>" + child.address + "</i>"
        description = description + "<p>"+ child.description + "</p>"
        createData(child.lon, child.lat, child.name, child.address, child.problems, description, desc_raw)
        num_points =  num_points + 1
      })
  })
}

removePoint.addEventListener('click', e => {
  var ref = database.ref('points/' + dataBlockName.innerText);
  ref.remove()
  dataBlockName.innerText = "Select Point on Map"
  dataBlockAddress.innerText = ""
  dataBlockDesc.innerText = ""
  window.location = "."
})

getPoints(function(results) {
  map.triggerRepaint()
})

addPointButton.addEventListener('click', e => {
  name = personNameInput.value
  address = addressInput.value
  problems = ""
  for (var i = 0; i < problemButtons.length; i++) {
    if (problemButtons[i].classList.contains("btn-danger")) {
      problems += "1"
    }
    else{
      problems += "0"
    }
  }
  description = descriptionInput.value
  coordinatesRequest(name, address, problems, description)

})
function locationWrite(name, address, problems, lon, lat, desc){
  firebase.database().ref('points/' + name).set({
    address: address,
    name: name,
    problems: problems,
    lon: lon,
    lat: lat,
    description: description
  })
}


var switchColor = function() {
  if (this.classList.contains("btn-secondary")) {
    this.classList.remove("btn-secondary")
    this.classList.add("btn-danger")
  }
  else{
    this.classList.remove("btn-danger")
    this.classList.add("btn-secondary")
  }
}


for (var i = 0; i < problemButtons.length; i++) {
    problemButtons[i].addEventListener('click', switchColor, false);
}



//var d = createData(-86.255, 41.679, "Chandler Crane", "203 E. Navarre Ave.", 1, "<strong>Truckeroo</strong><p><a href=\"http://www.truckeroodc.com/www/\" target=\"_blank\">Truckeroo</a> brings dozens of food trucks, live music, and games to half and M Street SE (across from Navy Yard Metro Station) today from 11:00 a.m. to 11:00 p.m.</p>")
//var z = createData(-86.259, 41.671, "Chandler Crane", "203 E. Navarre Ave.", 1, "<strong>Truckeroo</strong><p><a href=\"http://www.truckeroodc.com/www/\" target=\"_blank\">Truckeroo</a> brings dozens of food trucks, live music, and games to half and M Street SE (across from Navy Yard Metro Station) today from 11:00 a.m. to 11:00 p.m.</p>")
//var z = createData(-86.240, 41.671, "Chandler Crane", "203 E. Navarre Ave.", 1, "<strong>Truckeroo</strong><p><a href=\"http://www.truckeroodc.com/www/\" target=\"_blank\">Truckeroo</a> brings dozens of food trucks, live music, and games to half and M Street SE (across from Navy Yard Metro Station) today from 11:00 a.m. to 11:00 p.m.</p>")
//var z = createData(-86.240, 41.654, "Chandler Crane", "203 E. Navarre Ave.", 1, "<strong>Truckeroo</strong><p><a href=\"http://www.truckeroodc.com/www/\" target=\"_blank\">Truckeroo</a> brings dozens of food trucks, live music, and games to half and M Street SE (across from Navy Yard Metro Station) today from 11:00 a.m. to 11:00 p.m.</p>")
//var z = createData(-86.200, 41.724, "Chandler Crane", "203 E. Navarre Ave.", 1, "<strong>Truckeroo</strong><p><a href=\"http://www.truckeroodc.com/www/\" target=\"_blank\">Truckeroo</a> brings dozens of food trucks, live music, and games to half and M Street SE (across from Navy Yard Metro Station) today from 11:00 a.m. to 11:00 p.m.</p>")

map.on('load', function () {

  map.addImage('dot', dot, { pixelRatio: 2 });

  map.addLayer(data_points)
  totalCases.innerText = num_points

  // When a click event occurs on a feature in the places layer, open a popup at the
  // location of the feature, with description HTML from its properties.
  map.on('click', 'dots', function (e) {
    console.log(e.features[0])
    var name = e.features[0].properties.name
    console.log(name)
    var address = e.features[0].properties.address
    console.log(address)
    var cc = e.features[0].properties.cc
    var description = e.features[0].properties.description
    var desc_raw = e.features[0].properties.description_raw
    var coordinates = e.features[0].geometry.coordinates.slice()

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    new mapboxgl.Popup()
      .setLngLat(coordinates)
      .setHTML(description)
      .addTo(map);

    propogateDataBlock(name, address, cc, desc_raw)
  });


  // Change the cursor to a pointer when the mouse is over the places layer.
  map.on('mouseenter', 'dots', function () {
    map.getCanvas().style.cursor = 'pointer';
  });

  // Change it back to a pointer when it leaves.
  map.on('mouseleave', 'dots', function () {
    map.getCanvas().style.cursor = '';
  });

});

var accessToken = 'pk.eyJ1IjoiY2hhbmRsZXJjcmFuZSIsImEiOiJjanQwcTNhaWEwM2x2M3pwM2I4Nm40Nmo2In0.82_w067nYHhuvrsL3kuHUw';

function coordinatesRequest(name, address, problems, desc){
    var xhrCoords = new XMLHttpRequest() // creating request object
    var requestURL = "https://api.mapbox.com/geocoding/v5/mapbox.places/"
    requestURL = requestURL + address.trim().replace(/\s/g, '%20') + ".json" + "?proximity=-86.200, 41.724&access_token=" + accessToken
    console.log(requestURL)
    xhrCoords.open("GET", requestURL, true) // associates request attributes with XHR

    xhrCoords.onload = function(e) { // when response received...
        response = JSON.parse(xhrCoords.responseText)
        console.log(response)
        lon = response.features[0]['center'][0]
        lat = response.features[0]['center'][1]
        locationWrite(name, address, problems, lon, lat, desc)
    }

    xhrCoords.onerror = function(e){ // when error response received...
        console.error(xhrCoords.statusText)

    }

    xhrCoords.send(null) // send request
}
