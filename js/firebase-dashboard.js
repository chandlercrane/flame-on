// Get DOM elements
const logoutButton = document.getElementById('logoutButton')

// Add logout event
logoutButton.addEventListener('click', e => {
    const promise = firebase.auth().signOut()
    promise.catch(e => {
        console.log(e.code)
        console.log(e.message)
    })
})

// Add section changing code
const dashboardButton = document.getElementById('dashboardButton')
const settingsButton = document.getElementById('settingsButton')
const dashboardSection = document.getElementById('dashboardSection')
const settingsSection = document.getElementById('settingsSection')


// Section button switching logic
dashboardButton.addEventListener('click', e => {
    dashboardSection.style = "display:block"
    dashboardButton.classList.remove("sidebar-button-color-notselected")
    dashboardButton.classList.add("sidebar-button-color-selected")
    settingsSection.style = "display:none"
    settingsButton.classList.remove("sidebar-button-color-selected")
    settingsButton.classList.add("sidebar-button-color-notselected")
})
settingsButton.addEventListener('click', e => {
    dashboardSection.style = "display:none"
    dashboardButton.classList.remove("sidebar-button-color-selected")
    dashboardButton.classList.add("sidebar-button-color-notselected")
    settingsSection.style = "display:block"
    settingsButton.classList.remove("sidebar-button-color-notselected")
    settingsButton.classList.add("sidebar-button-color-selected")
})

firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser){
        console.log(firebaseUser)
    }
    else{
        console.log("not logged in")
        window.location = '..'
    }
})
