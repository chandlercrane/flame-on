(function(){

    // Get DOM elements
    const emailField = document.getElementById('emailField')
    const passwordField = document.getElementById('passwordField')
    const createUserButton = document.getElementById('createUserButton')
    const createUserAlert = document.getElementById('createUserAlert')

    // Add login event
    createUserButton.addEventListener('click', e => {
        const email = emailField.value
        const password = passwordField.value
        console.log("email: ", email)
        console.log("password: ", password)
        // Create User
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
            // Handle Errors here
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode)
            console.log(errorMessage)
            createUserAlert.classList.remove('alert-primary')
            createUserAlert.classList.add('alert-danger')
            createUserAlert.innerText = errorMessage
        });
    })

    firebase.auth().onAuthStateChanged(firebaseUser => {
        if(firebaseUser){
            console.log(firebaseUser)
		          window.location = '../dashboard'
        }
        else{
            console.log("not logged in")
        }
    })

}())
