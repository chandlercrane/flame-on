// Get DOM elements
const emailField = document.getElementById('emailField')
const passwordField = document.getElementById('passwordField')
const loginButton = document.getElementById('loginButton')
const logoutButton = document.getElementById('logoutButton')

// Add login event
loginButton.addEventListener('click', e => {
    const email = emailField.value
    const password = passwordField.value
    console.log("email: ", email)
    console.log("password: ", password)
    // Sign In
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
        // Handle Errors
        console.log(e.code)
        console.log(e.message)
        window.alert("Error: ", e.message)
    });
})

// Add logout event
logoutButton.addEventListener('click', e => {
    const promise = firebase.auth().signOut()
    promise.catch(e => {
        console.log(e.code)
        console.log(e.message)
    })
})

firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser){
        console.log(firebaseUser)
	    window.location = './dashboard'
    }
    else{
        console.log("not logged in")
    }
})
